import {expect} from 'chai';
import HeadersDescriptor from "../datagram/packets/headers/HeadersDescriptor";
import * as assert from "assert";
import Header from "../datagram/packets/headers/Header";


class EmptyHeader extends Header {
    constructor(offset: number, size: number) {
        super(offset, size);

    }

    read(rcvBuffer: Buffer): number {
        return 0;
    }

    write(sndBuffer: Buffer): number {
        return 0;
    }
}





let rcvBuffer: Buffer;
let sndBuffer: Buffer;

let target: HeadersDescriptor;

beforeEach("init descriptor", () => {

    rcvBuffer = Buffer.alloc(100);
    sndBuffer = Buffer.alloc(100);

    target = new HeadersDescriptor(rcvBuffer, rcvBuffer);
});


describe("Check headers placed correctly", () => {


    it("should allocate headers and load data", () => {
        //add header processor occupying [0-3] including
        target.addHeader(
            new EmptyHeader(0, 4)
        );

        let plainPacketBuffer: Buffer = target.packDataWithHeaders(Buffer.from("DATA"));

        assert.strictEqual(plainPacketBuffer.readUInt32BE(0), 0);
        assert.strictEqual(plainPacketBuffer.toString("UTF-8", 4, 8), "DATA")
    });


    it("should allocate 2 headers size", () => {
        //add header processor occupying [0-3] including
        target.addHeader(
            new EmptyHeader(0,4)
        );

        target.addHeader(
            new EmptyHeader(4,12)
        );

        let plainPacketBuffer: Buffer = target.packDataWithHeaders(Buffer.from("DATA"));

        assert.strictEqual(plainPacketBuffer.readUInt32BE(0), 0);
        assert.strictEqual(plainPacketBuffer.toString("UTF-8", 16, 20), "DATA")
    });


    it("should throw overlap exception", () => {
        //add header processor occupying [0-3] including

       expect(() => {
           target.addHeader(
               new EmptyHeader(0,4)
           );

           target.addHeader(
               new EmptyHeader(3,12)
           );
        }).to.throw(/Packet headers overlap detected/);

    });



    it("should allocate huge headers section and return null buffer for send", () => {
        target.addHeader(
            new EmptyHeader(50,10)
        );

        target.addHeader(
            new EmptyHeader(77,20)
        );


        let plainPacketBuffer: Buffer = target.packDataWithHeaders(Buffer.from("DATA"));


        assert.strictEqual(plainPacketBuffer, null)
    });

});
