
import HeadersDescriptor from "./packets/headers/HeadersDescriptor";
import {createSocket, RemoteInfo, Socket} from "dgram";
import CRC32Header from "./packets/headers/CRC32Header";


type MessageHandlerFunc = (msg: Buffer, rinfo: RemoteInfo) => void;

export default class UDPHostConnection{


    public socket: Socket;

    public port: number;


    public rcvBuffer: Buffer;
    public sndBuffer: Buffer;


    private headersDescriptor: HeadersDescriptor;


    private _onMessageFunc: MessageHandlerFunc;
    private _onListeningFunc: ()=>void;

    set onListeningFunc(value: () => void) {
        this._onListeningFunc = value;
    }
    set onMessageFunc(value: MessageHandlerFunc) {
        this._onMessageFunc = value;
    }

    constructor(port: number, packetSizeBytes: number){
        this.socket = createSocket("udp4");

        this.rcvBuffer = Buffer.alloc(packetSizeBytes);
        this.sndBuffer = Buffer.alloc(packetSizeBytes);


        this.headersDescriptor = new HeadersDescriptor( this.rcvBuffer, this.sndBuffer );

        this.headersDescriptor.addHeader(
            new CRC32Header()
        );


        this.socket.on("message", this.innerMessageHandler.bind(this));
        this.socket.on("listening", this.innerListeningHandler.bind(this));

        this.port = port;
    }


    private innerMessageHandler(plainPacketBuffer: Buffer, rinfo: RemoteInfo){
        // decline packets unable to read
        let loadDataBuffer: Buffer = this.headersDescriptor.validateReceivedPacket(plainPacketBuffer);

        // headers failed to validate.
        if( loadDataBuffer === null ){
            return;
        }

        //assume that this.onMessageFunc was bound ".bind()".
        this._onMessageFunc.call(null, loadDataBuffer, rinfo);
    }


    private innerListeningHandler(){
        this._onListeningFunc.apply(null, arguments);
    }



    public sendPacketLoad(packetLoad: Buffer, ipAddr: string, port: number){
        let sndPacketBuffer: Buffer = this.headersDescriptor.packDataWithHeaders(packetLoad);

        // failed to put packet load - too large load
        if( sndPacketBuffer === null ){
            return;
        }

        this.socket.send(sndPacketBuffer, 0, sndPacketBuffer.length, port, ipAddr);
    }

    public open():void{
        this.socket.bind(this.port);
    }
}
