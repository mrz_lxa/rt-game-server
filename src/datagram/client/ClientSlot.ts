import {ClientStatus} from "./ClientStatus";

const DEFAULT_ADDR: string = "localhost";

export default class ClientSlot{


    private _addr: string;
    private _port: number;
    private readonly _id: number;

    private _status: ClientStatus;

    constructor(slotId: number){
        this._id = slotId;
    }

    get status(): ClientStatus {
        return this._status;
    }

    set status(value: ClientStatus) {
        this._status = value;
    }


    get id(): number {
        return this._id;
    }
    get port(): number {
        return this._port;
    }
    get addr(): string {
        return this._addr;
    }
}