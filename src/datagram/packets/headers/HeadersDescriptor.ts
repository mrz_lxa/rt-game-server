import Header from "./Header";


export default class HeadersDescriptor{

    private loadOffset: number;

    private rcvBuffer: Buffer;
    private sndBuffer: Buffer;

    private rcvLoadBuffer: Buffer;
    private sndLoadBuffer: Buffer;

    private headers: Array<Header>;


    constructor(rcvBuffer: Buffer, sndBuffer: Buffer){
        this.rcvBuffer = rcvBuffer;
        this.sndBuffer = sndBuffer;

        this.headers = [];

        this.reInitLoadBuffers();
    }


    public getAllowedLoadSize(){
        return this.sndLoadBuffer.length;
    }


    private reInitLoadBuffers(){
        this.loadOffset = this.calculateHeadersSectionSize();

        if(this.loadOffset < 0){
            throw new Error("Packet headers overlap detected");
        }

        this.rcvLoadBuffer = this.rcvBuffer.slice(this.loadOffset);
        this.sndLoadBuffer = this.sndBuffer.slice(this.loadOffset);
    }


    public addHeader(header: Header){
        this.headers.push(header);

        this.reInitLoadBuffers();
    }


    /**
     * Checks for headers to over
     */
    private calculateHeadersSectionSize(): number{

        if(this.headers.length === 0){
            return 0;
        }

        let minStart: number = this.headers[0].offset;
        let maxEnd: number = minStart + this.headers[0].size;

        for(let i=0; i<this.headers.length; i++){

            const header: Header = this.headers[i];

            let offset: number = header.offset;
            let end: number = offset + header.size;


            //start of a new header is in occupied area.
            if(offset > minStart && offset < maxEnd){
                return -1;
            }

            //end of a new header is in occupied area.
            if(end > minStart && end < maxEnd){
                return -1;
            }

            //curr header tries to eat occupied area
            if(offset < minStart && end > maxEnd){
                return -1;
            }


            if(offset <= minStart){
                minStart = offset;
            }

            if(maxEnd < end){
                maxEnd = end;
            }

        }

        return maxEnd;
    }


    private performHeadersRead(rcvBuffer: Buffer): number{
        let bytesRead: number = 0;

        for(let i=0; i<this.headers.length; i++){
            const header: Header = this.headers[i];
            let headerReadResult: number;

            if((headerReadResult = header.read(rcvBuffer)) < 0){
                return -1;
            }

            bytesRead += headerReadResult;
        }

        return bytesRead;
    }


    private performHeadersWrite(sndBuffer: Buffer): number{
        let bytesWritten: number = 0;

        for(let i=this.headers.length-1; i>=0; i--){
            const header: Header = this.headers[i];
            let headerWriteResult: number;

            if((headerWriteResult = header.write(sndBuffer)) < 0){
                return -1;
            }

            bytesWritten += headerWriteResult;
        }


        return bytesWritten;
    }



    public validateReceivedPacket(plainPacketBuffer: Buffer):Buffer{
        //decline packets unable to read
        if( plainPacketBuffer.length > this.rcvBuffer.length ){
            console.debug(`Wrong incoming packet size. Allowed <= ${this.rcvBuffer.length}`);
            return null;
        }

        plainPacketBuffer.copy(this.rcvBuffer);

        if( this.performHeadersRead(this.rcvBuffer) < 0 ){
            console.debug("Skipping packet. One of the headers failed to validate");
            return null;
        }


        return this.rcvLoadBuffer;
    }


    public packDataWithHeaders(sndLoadData: Buffer): Buffer{

        if(sndLoadData.length > this.sndLoadBuffer.length){
            console.error("Trying to send load greater than allowed");
            return null;
        }

        sndLoadData.copy(this.sndLoadBuffer);

        this.sndLoadBuffer.writeUInt32LE(98321123, sndLoadData.length);
        this.performHeadersWrite(this.sndBuffer);

        return this.sndBuffer;
    }



}
