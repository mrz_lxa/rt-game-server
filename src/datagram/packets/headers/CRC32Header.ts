import Header from "./Header";
import {crc32} from "../../../crypto/CRC32"

export default class CRC32Header extends Header{

    public static readonly CRC32_HEADER_OFFSET = 0;
    public static readonly CRC32_HEADER_SIZE = 4;

    constructor(){
        super(CRC32Header.CRC32_HEADER_OFFSET, CRC32Header.CRC32_HEADER_SIZE);
    }


    read(rcvBuffer: Buffer): number {
        //compare read hash sum with calculated hash sum
        if(rcvBuffer.readUInt32LE(this.offset) !== crc32(rcvBuffer, this.offset+this.size)){
            return -1;
        }

        return this.size;
    }

    write(sndBuffer: Buffer): number {
        //calc crc32 of remaining buffer
        let checksum = crc32(sndBuffer, this.offset + this.size);
        sndBuffer.writeUInt32LE(checksum, 0);
        return this.size;
    }

}
