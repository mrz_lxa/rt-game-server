import FakeClient from "./FakeClient";


const client: FakeClient = new FakeClient("127.0.0.1", 2222);

let counter = 0;

setInterval(()=>{
    counter++;
    client.sendMessage(Buffer.from("FAKE CLIENT MESSAGE. (" + counter + ")"));
}, 1000);